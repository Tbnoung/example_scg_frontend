import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Modal: false,
    DataMachine: {
      title: 'เพิ่มเครื่องในระบบ',
      id: '',
      location: '',
      discription: '',
      slot: '1',
      email: '',
      status: 'create'
    },
    DataUser: {
      username: '',
      password: '',
      firstname: '',
      lastname: '',
      role: 'admin',
      title: 'เพิ่มผู้ใช้งาน',
      status: 'create'
    },
    DataProduct: {
      id: '',
      product_name: '',
      price: '',
      qty: '',
      status: 'create',
      image: '',
      title: 'เพิ่มรายการสินค้า'
    }
  },
  mutations: {
    SetModal (state) {
      state.Modal = !state.Modal
    },
    SetDataMachine (state, data) {
      state.DataMachine = data
    },
    SetDataUser (state, data) {
      state.DataUser = data
    },
    SetDataProduct (state, data) {
      state.DataProduct = data
      // console.log('DataProduct', state.DataProduct)
    }
  },
  actions: {
  },
  modules: {
  }
})
