const CreateReport = (val) => {
  var report = {
    query: `mutation  {
      CreateReport (
        id_machine: "${val.id_machine}"
        id_product: "${val.id_product}"
        location: "${val.location}"
        discription: "${val.discription}"
        product_name: "${val.product_name}"
        qty: "${val.qty}"
        price: "${val.price}"
      ) {
        code
        result
        message
      }
    }`
  }
  return report
}
const GetAllReport = () => {
  var report = {
    query: `query  {
      GetAllReport {
        code
        data{
          product_name
          qty
        }
        result
        message
      }
    }`
  }
  return report
}
module.exports = { CreateReport, GetAllReport }
