const Login = (val) => {
  var UserOneID = {
    query: `query  {
      Login (username: "${val.username}", password: "${val.password}") {
        code
        data {
          id
          username
          firstname
          lastname
          role
          token
        }
        result
        message
      }
    }`
  }
  return UserOneID
}
const GetAllUSer = () => {
  var User = {
    query: `query  {
      GetAllUser {
        code
        data {
          id
          username
          role
          firstname
          lastname
        }
        result
        message
      }
    }`
  }
  return User
}
const CreateUser = (val) => {
  var User = {
    query: `mutation  {
      Register(
        username: "${val.username}"
        password: "${val.password}"
        firstname: "${val.firstname}"
        lastname: "${val.lastname}"
        role: "${val.role}"
      ) {
        code
        result
        message
      }
    }`
  }
  return User
}
const UpdateUser = (val) => {
  var User = {
    query: `mutation  {
      UpdateUser(
        id: "${val.id}"
        username: "${val.username}"
        firstname: "${val.firstname}"
        lastname: "${val.lastname}"
      ) {
        code
        result
        message
      }
    }`
  }
  return User
}
module.exports = { Login, GetAllUSer, CreateUser, UpdateUser }
