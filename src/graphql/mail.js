const SendMail = (val) => {
  var Mail = {
    query: `mutation  {
      SendMail (
        product_name: "${val.product_name}"
        location: "${val.location}"
        discription: "${val.discription}"
        qty: "${val.qty}"
        price: "${val.price}"
        email: "${val.email}"
      ) {
        code
        result
        message
      }
    }`
  }
  return Mail
}
module.exports = { SendMail }
