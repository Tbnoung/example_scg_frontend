const SaveImage = (val) => {
  var Machine = {
    query: `mutation  {
      CreateImage (
        image: "${val}"
      ) {
        data
        code
        result
        message
      }
    }`
  }
  return Machine
}
module.exports = { SaveImage }
