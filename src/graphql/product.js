const GetAllProduct = () => {
  var product = {
    query: `query  {
      GetAllProduct {
        code
        data {
          image
          id
          product_name
          price
          qty
        }
        result
        message
      }
    }`
  }
  return product
}
const CreateProduct = (val) => {
  var product = {
    query: `mutation  {
      CreateProduct (
        token: "${val.token}"
        product_name: "${val.product_name}"
        price: "${val.price}"
        qty: "${val.qty}"
        image: "${val.image}"
      ) {
        code
        data {
          id
          product_name
          price
          qty
        }
        result
        message
      }
    }`
  }
  return product
}
const UpdateProduct = (val) => {
  var product = {
    query: `mutation  {
      UpdateProduct (
        id: "${val.id}"
        product_name: "${val.product_name}"
        price: "${val.price}"
        qty: "${val.qty}"
        image: "${val.image}"
      ) {
        code
        result
        message
      }
    }`
  }
  return product
}
module.exports = { GetAllProduct, CreateProduct, UpdateProduct }
