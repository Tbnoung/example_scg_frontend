const GetAllMachine = () => {
  var Machine = {
    query: `query  {
      GetAllMachine {
        code
        data {
          id
          discription
          location
          slot
          create_by
          update_by
          machine_product
          username
          email
        }
        result
        message
      }
    }`
  }
  return Machine
}
const CreateMachine = (val) => {
  var Machine = {
    query: `mutation  {
      CreateMachine (
        token: "${val.token}"
        location: "${val.location}"
        discription: "${val.discription}"
        slot: "${val.slot}"
        email: "${val.email}"
        update_by: ""
        machine_product: "${val.machine_product}"
      ){
        code
        result
        message
      }
    }`
  }
  return Machine
}
const GetMachineByID = (val) => {
  var Machine = {
    query: `query  {
      GetMachineByID (
        id: "${val}"
      ) {
        code
        data {
          id
          id_user
          discription
          location
          slot
          create_by
          update_by
          machine_product
          username
          email
        }
        result
        message
      }
    }`
  }
  return Machine
}
const GetMachineByIDUser = (val) => {
  var Machine = {
    query: `query  {
      GetMachineByIDUser (
        id: "${val}"
      ) {
        code
        data {
          id
          id_user
          discription
          location
          slot
          create_by
          update_by
          machine_product
          username
          email
        }
        result
        message
      }
    }`
  }
  return Machine
}
const UpdateMachineProduct = (val) => {
  var Machine = {
    query: `mutation  {
      UpdateMachineProduct (
        id_machine: "${val.id_machine}"
        id_product: "${val.id_product}"
        qty: "${val.qty}"
        data: "${val.data}"
      ) {
        code
        result
        message
      }
    }`
  }
  return Machine
}
const UpdateMachineByID = (val) => {
  var Machine = {
    query: `mutation  {
      UpdateMachine (
        id: "${val.id}"
        location: "${val.location}"
        discription: "${val.discription}"
        email: "${val.email}"
      ) {
        code
        result
        message
      }
    }`
  }
  return Machine
}
module.exports = { GetAllMachine, CreateMachine, GetMachineByID, UpdateMachineProduct, GetMachineByIDUser, UpdateMachineByID }
