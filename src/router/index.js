import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/homepage',
    component: () => import('@/views/Homepage'),
    children: [
      { path: '/homepage', name: 'homepage', component: () => import('@/views/Dashboard') },
      { path: '/user', name: 'user', component: () => import('@/views/User') },
      { path: '/machine', name: 'machine', component: () => import('@/views/Machine') },
      { path: '/machinedetail', name: 'machinedetail', component: () => import('@/views/MachineDetail') },
      { path: '/product', name: 'product', component: () => import('@/views/Product') }
    ]
  },
  { path: '/machineseller', name: 'machineseller', component: () => import('@/views/MachineSeller') },
  { path: '/', name: 'login', component: () => import('@/views/Login') }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
